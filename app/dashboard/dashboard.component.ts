import { Component } from '@angular/core';

import { SidebarComponent } from '../sidebar/sidebar.component';

@Component ({
    moduleId: module.id,
    selector: 'dashboard',
    templateUrl: './dashboard.component.html',
    // directives: [SidebarComponent]
})

export class DashboardComponent {}
