import {Component} from '@angular/core';
import { Router } from '@angular/router';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { SidebarComponent } from '../sidebar/sidebar.component'

@Component ({
    moduleId: module.id,
    selector: 'register',
    templateUrl: './register.component.html'
})

export class RegisterComponent {
    constructor (private router: Router){ }

    redirect(){
        this.router.navigate(['./dashboard']);
    }
}

// platformBrowserDynamic().bootstrapModule(SidebarComponent);