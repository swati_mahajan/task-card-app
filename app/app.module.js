"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// import {Component} from '@angular/core';
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
// import {nvD3} from 'ng2-nvd3';
// declare let d3: any;
var app_component_1 = require('./app.component');
var card_component_1 = require('./card/card.component');
var login_component_1 = require('./login/login.component');
var register_component_1 = require('./register/register.component');
var dashboard_component_1 = require('./dashboard/dashboard.component');
var sidebar_component_1 = require('./sidebar/sidebar.component');
var appRoutes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'login', component: login_component_1.LoginComponent, pathMatch: 'full' },
    { path: 'register', component: register_component_1.RegisterComponent, data: { title: 'Registration Page' } },
    { path: 'dashboard', component: dashboard_component_1.DashboardComponent, data: { title: 'Dashboard Page' } },
    // { path: 'sidebar', component: SidebarComponent, data: { title: 'sidebar Page' }},
    { path: 'my-app', component: sidebar_component_1.SidebarComponent, data: { title: 'app Page' } }
];
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                router_1.RouterModule.forRoot(appRoutes)
            ],
            declarations: [
                app_component_1.AppComponent,
                card_component_1.CardComponent,
                login_component_1.LoginComponent,
                register_component_1.RegisterComponent,
                dashboard_component_1.DashboardComponent,
                sidebar_component_1.SidebarComponent
            ],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map