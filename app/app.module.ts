// import {Component} from '@angular/core';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
// import {nvD3} from 'ng2-nvd3';
// declare let d3: any;

import { AppComponent } from './app.component';
import { CardComponent } from './card/card.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import {  DashboardComponent } from './dashboard/dashboard.component';
import { SidebarComponent } from './sidebar/sidebar.component';

const appRoutes: Routes = [
    { path: '',   redirectTo: '/login', pathMatch: 'full' },
    { path: 'login', component: LoginComponent, pathMatch: 'full'},
    { path: 'register', component: RegisterComponent, data: { title: 'Registration Page' }},
    { path: 'dashboard', component: DashboardComponent, data: { title: 'Dashboard Page' }},
    // { path: 'sidebar', component: SidebarComponent, data: { title: 'sidebar Page' }},
    { path: 'my-app', component: SidebarComponent, data: { title: 'app Page' }}

];

@NgModule({
    imports: [
         BrowserModule,
         FormsModule,
         RouterModule.forRoot(appRoutes)
          ],
    declarations: [ 
        AppComponent,
        CardComponent,
        LoginComponent,
        RegisterComponent,
        DashboardComponent,
        SidebarComponent
        // nvD3
         ],
       bootstrap:[ AppComponent ]
    // bootstrap: [SidebarComponent,AppComponent ]
})

export class AppModule { }