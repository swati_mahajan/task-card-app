import { Component } from '@angular/core';

import { Task } from  './model/task';
// import { LoginComponent } from './login/login.component';

@Component({
    moduleId: module.id,
    selector: 'my-app',
    template: `<router-outlet></router-outlet>`,
    styleUrls: ['app.component.css']
})
export class AppComponent {
    private tasks: Task[] = [
    ]

    private currentTasks = new Task(null,false);

    addTask() {
        let task = new Task(this.currentTasks.content,this.currentTasks.completed);
        this.tasks.push(task);
        this.currentTasks.content= null;
    }
}